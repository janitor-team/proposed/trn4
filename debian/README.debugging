When trn is compiled with -DDEBUG (as the Debian package is), you can use
the -D switch to enable a variety of debugging options. These are defined in
common.h in the source, but for reference I'll explain them here. -D is
followed (without a space) by a number, which is the sum of any of several
options:

      2 DEB_COREDUMPSOK		dump core if trn crashes
      4 DEB_HEADER		show parsed header information as it is read
      8 DEB_INTRP		verbose interpretation of % escapes etc.
     16 DEB_NNTP		show NNTP transactions
     32 DEB_INNERSRCH		debug searches within articles
     64 DEB_FILEXP		verbose filename expansion
    128 DEB_HASH		not used
    256 DEB_XREF_MARKER		debug marking of cross-posted articles
    512 DEB_CTLAREA_BITMAP	show parsing of .newsrc lines into bitmaps
   1024 DEB_RCFILES		show parsing of trnrc and similar files
   2048 DEB_NEWSRC_LINE		show construction of .newsrc lines
   4096 DEB_SEARCH_AHEAD	debug searches among articles
   8192 DEB_CHECKPOINTING	show .newsrc checkpointing
  16384 DEB_FEED_XREF		not used

Thus, -D2066 would show .newsrc construction and NNTP transactions, as well
as allowing core dumps if system resource limits permit it.

 -- Colin Watson <cjwatson@debian.org>  Wed, 14 Feb 2001 01:46:33 +0000
